"""
Library of functions for reading, transforming, and writing the data.

Copyright 2019 Regents of the University of Michigan

This software is the result of collaborative efforts from all
participants of the C2Metadata project (http://www.c2metadata.org)

C2Metadata is supported by the Data Infrastructure Building
Blocks (DIBBs) program of the National Science Foundation through
grant NSF ACI-1640575.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

File created in 2019 by Alexander Mueller.
"""

import json
import pandas as pd

# TODO: clone the JSON from gitlab into the current directory, finish documentation

# All of this is stored in separate data structures to allow for easier editing
sparse = {}
functions = {}
row_nsu = { "horizontal": [], "vertical": [], "collapse": [], "logical": [] }
row_parameters = { "horizontal": [], "vertical": [], "collapse": [], "logical": [] }
states = ["horizontal", "vertical", "collapse", "logical"]
languages = ["SDTL", "SPSS", "Stata", "SAS", "R", "Python"]

def read_json(json_path):
    """
    Load the json file into a dictionary of pandas objects.
    """
    json_dict = {}

    # clear existing objects
    sparse.clear()
    functions.clear()
    for key in row_nsu.keys():
        row_nsu[key].clear()
    for key in row_parameters.keys():
        row_parameters[key].clear()

    with open(json_path, "r") as json_file:
        json_dict = json.load(json_file)
    
    for state in states:
        try:
            functions[state] = pd.DataFrame(data=json_dict[state]).set_index("SDTLname")
            sparse[state] = functions[state][["n_arguments", "returnType", "definition", "notes"]].copy()
            sparse[state].fillna("", inplace=True)
            #sparse[state] = sparse[state].astype(str)
        except KeyError:
            functions[state] = sparse[state] = pd.DataFrame()
    
    for state in states:
        expand_parameters(state=state)

def write_json(json_path):
    """
    Write the dictionary of pandas options back to the JSON file.
    """
    json_dict = {}
    for state in states:
        json_dict[state] = collapse_parameters(state=state)

    with open(json_path, "w") as json_file:
        json.dump(json_dict, json_file, indent=4)
    functions.clear()

def is_empty_string(cell):
    if isinstance(cell, str):
        return len(cell) == 0
    return False

# Collapse all of the data structures into a JSON array for each state
def collapse_parameters(state):
    #sparse[state].replace(r'^\s*$', None, regex=True, inplace=True)
    sparse[state] = sparse[state].applymap(lambda x: None if is_empty_string(x) else x)
    state_functions = sparse[state].reset_index().to_dict("records")
    for row in range(len(state_functions)):
        nsu_df = row_nsu[state][row]
        #nsu_df.replace(r'^\s*$', None, regex=True, inplace=True)
        nsu_df = nsu_df.applymap(lambda x: None if is_empty_string(x) else x)
        parameters_list = row_parameters[state][row]

        for language in languages:
            if language != "SDTL":
                state_functions[row][language] = []

        nsu_dict = nsu_df.to_dict()
        for index, column in enumerate(nsu_df.columns):
            parameters = parameters_list[index]
            #parameters.replace(r'^\s*$', None, regex=True, inplace=True)
            parameters = parameters.applymap(lambda x: None if is_empty_string(x) else x)
            function_dict = nsu_dict[column]
            function_dict["parameters"] = []

            for exp in parameters.columns:
                function_dict["parameters"].append(parameters[exp].to_dict())

            if "_" in column:
                state_functions[row][column[:column.index("_")]].append(function_dict)
            elif column == "SDTL":
                function_dict["Pseudocode"] = function_dict.pop("syntax")
                function_dict["SDTLname"] = function_dict.pop("function_name")
                function_dict.pop("URL")
                state_functions[row].update(function_dict)
            else:
                state_functions[row][column].append(function_dict)
    return state_functions

# Reshape the contents of the input JSON into a more usable format
def expand_parameters(state):
    for row in range(len(functions[state])):
        expand_row(row, state)

# For the given row, populate the NSU and parameter dicts with reshaped info
def expand_row(row, state, position=None):
    nsu_df = get_name_syntax_url(row, state=state)
    parameters_df = nsu_df.loc[["parameters"]]
    parameters_list = []
    for column in parameters_df.columns:
        parameter_list = parameters_df[column]["parameters"]
        parameter_df = pd.DataFrame(parameter_list).T
        parameter_df.columns = ["EXP" + str(int(column_name) + 1) for column_name in parameter_df.columns]
        parameter_df.fillna("", inplace=True)
        #parameter_df = parameter_df.astype(str)
        parameters_list.append(parameter_df)
    nsu_df.drop("parameters", inplace=True)
    nsu_df.fillna("", inplace=True)
    #nsu_df = nsu_df.astype(str)
    if position is None:
        row_nsu[state].append(nsu_df)
        row_parameters[state].append(parameters_list)
    else:
        row_nsu[state].insert(position, nsu_df)
        row_parameters[state].insert(position, parameters_list)

# Reshape the Name, Syntax, and URL information for the given row
def get_name_syntax_url(row, state):
    simple_df = functions[state].drop(["n_arguments", "returnType", "definition", "notes"], axis="columns")
    simple_df.reset_index(inplace=True)
    SDTL = [
        [
            {
                "function_name": simple_df.iloc[[row]]["SDTLname"].iloc[0], 
                "syntax": simple_df.iloc[[row]]["Pseudocode"].iloc[0], 
                "parameters": simple_df.loc[row]["parameters"], 
                "URL": ""
            }
        ] for row in simple_df.index
    ]
    simple_df["SDTL"] = SDTL
    simple_df.drop(["SDTLname", "Pseudocode"], axis="columns", inplace=True)
    nsu_list = []
    columns = []
    for language in languages:
        nsu_cell = simple_df[language].iloc[row]
        for index, function in enumerate(nsu_cell):
            nsu_series = pd.Series(function)
            nsu_list.append(nsu_series)
            if(len(nsu_cell) > 1):
                columns.append(language + "_" + str(index+1))
            else:
                columns.append(language)
    nsu_df = pd.concat(nsu_list, axis=1, sort=False)
    nsu_df.columns = columns
    return nsu_df

# Insert a new parameter dataframe into the list for that row at position
def insert_parameters(row, position, state):
    neighbor = row_parameters[state][row][position - 1]
    parameters = pd.DataFrame(columns=neighbor.columns, index=neighbor.index, data="")
    row_parameters[state][row].insert(position, parameters)

# Remove the parameter dataframe at position from the list for that row
def remove_parameters(row, position, state):
    row_parameters[state][row].pop(position)

# Remove the contents of both the parameters and NSU dicts for the given row
def delete_row(row, state):
    row_parameters[state].pop(row)
    row_nsu[state].pop(row)

# Insert new parameter and NSU information into the dicts for the given row
def insert_row(row, state):
    neighbor_nsu = row_nsu[state][row]
    nsu_df = pd.DataFrame(columns=neighbor_nsu.columns, index=neighbor_nsu.index, data="")
    row_nsu[state].insert(row, nsu_df)

    neighbor_parameters = row_parameters[state][row]
    parameters_list = []
    for neighbor_df in neighbor_parameters:
        parameter_df = pd.DataFrame(columns=neighbor_df.columns, index=neighbor_df.index, data="")
        parameters_list.append(parameter_df)
    row_parameters[state].insert(row, parameters_list)
