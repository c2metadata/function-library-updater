# Function Library Updater

The SDTL Function Library used to be stored in a JSON document and maintained via a spreadsheet. This application replaced the spreadsheet with a custom graphical user interface to update of the function library.

The eventual plan is for any changes made using this application to either update the existing [JSON document](https://gitlab.com/c2metadata/SDTL-translation-libraries/tree/master/SDTL_Function_Library/SDTL_Function_Library.json) upon successful entry of GitLab credentials, or make a merge request on the document to facilitate discussion.

# Installation

First, check that your Python version is at least `3.8.1`. Lower versions may work, but they have not been tested.

Then, run the following command to install the necessary dependencies:
```
pip3 install pyqt5 pandas
```
Finally, navigate to the `function-library-updater` directory and run the program with:
```
python3 gui.py
```
You should now see a window with a spreadsheet-style editor. If not, please file a bug report.

# Testing

By default, the application loads the version of `SDTL_Function_Library.json` present here. If you would like to test with a different JSON file instead, go to File -> Open and choose the file you wish to load. Please note that only valid JSON files that fit the new schema are supported.

The expected functionality of the spreadsheet views is as follows: when a new JSON document is loaded, there should be a view on the left with four columns and one row for each function. When a row is highlighted, a new spreadsheet should appear on the right side with three rows and one column for each language (if there is more than one variant in a given language, columns names for that language are followed by an underscore and an index). When a column (or cell) is highlighted in the view on the right, a new spreadsheet should appear below it with three rows and a column for each parameter (named `EXP1`, `EXP2`, etc.).

To remove the current row in the library view (on the left), go to Edit -> Delete Row. This will remove the row containing the currently selected cell. Similarly, Edit -> Insert Row will insert a new row at the currently selected position, and Edit -> Rename Row will rename the row of the selected cell.

To add a new function variant in the "Name, Syntax, URL" view (upper right if a row is selected on the left), go to Edit -> Add Function Variant and choose the language of the variant. Edit -> Remove Function Variant works in much the same way. In the parameters view (lower right if a column (or cell) is selected in the upper right), new parameters can be added via the "Add Parameter" option in the Edit menu. This will add a parameter to all function variants for the given SDTL function, not just the one selected. Edit -> Remove Parameter works in much the same way.

To test the application, make any changes you wish in the spreadsheet views, then go to File -> Save and enter a name for the new file. You will have to manually check whether the new file contains the changes you made in the UI. If it does not, please file a bug report.

# Project development

TODO:
- Research technologies for integration with GitLab authentication services

Done:
- Update README
- Create a user interface using [PyQt5](https://pypi.org/project/PyQt5/)
- Use [Python Pandas](https://pandas.pydata.org/) internally to do the data transformation
- Implement various features related to insertion, deletion, and renaming in all views (except where irrelevant)
- Update to the new format
