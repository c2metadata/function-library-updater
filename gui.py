"""
Graphical User Interface for modifying the function libraries.

Copyright 2019 Regents of the University of Michigan

This software is the result of collaborative efforts from all
participants of the C2Metadata project (http://www.c2metadata.org)

C2Metadata is supported by the Data Infrastructure Building
Blocks (DIBBs) program of the National Science Foundation through
grant NSF ACI-1640575.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

File created in 2019 by Alexander Mueller.
"""

import sys
import pandas as pd

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

import data_transform as dt

# The main window of the application
class MainWindow(QMainWindow):
    def __init__(self, table_widget):
        QMainWindow.__init__(self)
        self.setWindowTitle("Function Library Updater")

        # Throughout this UI, "state" refers to the library on display,
        # which is one of horizontal, vertical, collapse, logical
        self.state = "horizontal"

        # Create the menu bar
        self.menu = self.menuBar()

        # Map of menu display text to signals to call or submenus to display
        menus = {
            "File": {
                "Open": self.open,
                "Save": self.save,
                "Print": self.print_data,
                "Quit": self.close
            },
            "Edit": {
                "Rename Row": self.rename,
                "Insert Row": self.insert,
                "Delete Row": self.delete,
                "Add Parameter": self.add_parameter,
                "Remove Parameter": self.remove_parameter,
                "Add Function Variant": {
                    "SPSS": self.add_spss_variant,
                    "Stata": self.add_stata_variant,
                    "SAS": self.add_sas_variant,
                    "R": self.add_r_variant,
                    "Python": self.add_python_variant
                },
                "Remove Function Variant": {
                    "SPSS": self.remove_spss_variant,
                    "Stata": self.remove_stata_variant,
                    "SAS": self.remove_sas_variant,
                    "R": self.remove_r_variant,
                    "Python": self.remove_python_variant
                }
            },
            "Load": {
                "Compute (horizontal) functions": self.load_horizontal,
                "Aggregate (vertical) functions": self.load_vertical,
                "Collapse dataset": self.load_collapse,
                "Logical": self.load_logical
            }
        }

        # Connect the menus and actions declared above
        for menu, options in menus.items():

            # Quite unintuitively, you have to add the menu to its parent
            # before you add the child options to the current menu
            current_menu = self.menu.addMenu(menu)
            for option, value in options.items():

                # In our data structure, a dictionary value means a submenu
                if isinstance(value, dict):
                    submenu = current_menu.addMenu(option)

                    # Add each item to the submenu
                    for suboption, subvalue in value.items():
                        action = QAction(suboption, self)
                        action.triggered.connect(subvalue)
                        submenu.addAction(action)

                # If value is not a dict, treat as a regular menu item
                else:
                    action = QAction(option, self)
                    action.triggered.connect(value)
                    current_menu.addAction(action)

        # Set the window to be full screen and nonresizable
        geometry = qApp.desktop().availableGeometry(self)
        self.setFixedSize(geometry.width(), geometry.height())

        self.load_widget(table_widget)

    # Define a horizontal layout and add the table widget as the first item
    def load_widget(self, table_widget):
        self.table_widget = table_widget
        self.layout = QHBoxLayout()
        self.layout.addWidget(self.table_widget)
        self.central_widget = QWidget()
        self.central_widget.setLayout(self.layout)
        self.setCentralWidget(self.central_widget)

    # To facilitate easy Edit menu access, many of the functions that follow
    # are merely wrappers around those buried deep within the various views

    def add_variant(self, language):
        try:
            self.table_widget.nsu_widget.add_variant(language)
        except:
            QMessageBox.about(self, "Unsupported Operation", "The name, syntax, URL view must be loaded to add a variant.")

    def remove_variant(self, language):
        try:
            self.table_widget.nsu_widget.remove_variant(language)
        except:
            QMessageBox.about(self, "Unsupported Operation", "The name, syntax, URL view must be loaded to remove a variant.")

    # TODO: use lambdas or something to clean this up

    def add_spss_variant(self):
        self.add_variant("SPSS")

    def add_stata_variant(self):
        self.add_variant("Stata")

    def add_sas_variant(self):
        self.add_variant("SAS")

    def add_r_variant(self):
        self.add_variant("R")

    def add_python_variant(self):
        self.add_variant("Python")

    def remove_spss_variant(self):
        self.remove_variant("SPSS")

    def remove_stata_variant(self):
        self.remove_variant("Stata")

    def remove_sas_variant(self):
        self.remove_variant("SAS")

    def remove_r_variant(self):
        self.remove_variant("R")

    def remove_python_variant(self):
        self.remove_variant("Python")

    def add_parameter(self):
        try:
            self.table_widget.nsu_widget.parameter_widget.add_parameter()
        except AttributeError:
            QMessageBox.about(self, "Unsupported Operation", "The parameter view must be loaded to add a parameter.")

    def remove_parameter(self):
        try:
            self.table_widget.nsu_widget.parameter_widget.remove_parameter()
        except AttributeError:
            QMessageBox.about(self, "Unsupported Operation", "The parameter view must be loaded to remove a parameter.")

    # Prints the current tab of the function library to the console
    def print_data(self):
        print(dt.sparse[self.state])

    # Open a JSON file chosen by the user and send it off to be processed by dt
    def open(self):
        filename = QFileDialog.getOpenFileName(self, "Open existing file", ".", "JSON files (*.json)")
        print("Loading JSON file " + filename[0])
        dt.read_json(json_path=filename[0])
        self.load_widget(LibraryWidget(dt.sparse["horizontal"], "horizontal"))

    # Save the updated JSON file to a location of the user's choice
    def save(self):
        filename = QFileDialog.getSaveFileName(self, "Save file as", ".", "JSON files (*.json)")
        dt.write_json(json_path=filename[0])

    def insert(self):
        self.table_widget.insert()

    def rename(self):
        name, ok = QInputDialog.getText(self, "Rename row", "new name:")
        if ok:
            self.table_widget.rename(name)

    def delete(self):
        self.table_widget.remove()

    # Reset the layout with the another tab of the function library
    def load(self, state):
        self.clear_layout()
        self.state = state
        self.load_widget(LibraryWidget(dt.sparse[self.state], self.state))

    # TODO: again with the lambdas

    def load_horizontal(self):
        self.load("horizontal")

    def load_vertical(self):
        self.load("vertical")

    def load_collapse(self):
        self.load("collapse")
        
    def load_logical(self):
        self.load("logical")

    # remove all items currently in the layout
    def clear_layout(self):
        for x in reversed(range(self.layout.count())):
            item = self.layout.itemAt(x)
            if(isinstance(item, QtWidgets.QWidgetItem)):
                item.widget().close()
            self.layout.removeItem(item)


# Allows for custom definition of how to handle table operations in our views
class CustomTableModel(QAbstractTableModel):
    def __init__(self, data, state):
        QAbstractTableModel.__init__(self)
        self.table = data
        self.state = state

    # Some of this is boilerplate from Qt tutorials that doesn't need changing

    def rowCount(self, parent=QModelIndex()):
        return len(self.table.index)

    def columnCount(self, parent=QModelIndex()):
        return len(self.table.columns)

    def headerData(self, section, orientation, role):
        if role != Qt.DisplayRole:
            return None
        if orientation == Qt.Horizontal:
            return self.table.columns[section]
        else:
            return self.table.index[section]

    # Rename the given row (Qt.Vertical) or column (Qt.Horizontal) to value
    def setHeaderData(self, section, orientation, value, role=Qt.EditRole):
        if role == Qt.EditRole:

            # Renaming a column is straightforward
            if orientation == Qt.Horizontal:
                self.table.rename(columns={ (self.table.columns[section]): value }, inplace=True)
            
            # However, we need custom handling of row renames to prevent one
            # rename operation from renaming multiple rows sharing the old name
            else:
                index = self.table.index.values
                index[section] = value
                self.table.index = pd.Index(index, name="SDTLname")

            # This tells Qt to update the header in the view to match the data
            self.headerDataChanged.emit(orientation, section, section)

            return True     # The header was changed

        return False        # The header was not changed

    # Removing rows is pretty straightforward
    def removeRows(self, position, rows, parent):
        self.beginRemoveRows(QModelIndex(), position, position+rows-1)
        self.table.drop(self.table.index[position], inplace=True)
        self.endRemoveRows()
        return True         # The row was removed successfully

    # Inserting rows, however, requires some index manipulation to ensure
    # that the insertion happens at the right place instead of at the end
    def insertRows(self, position, rows, parent):

        # First, we tell Qt that the row insertion process has started
        self.beginInsertRows(QModelIndex(), position, position+rows-1)

        # Next, switch to a numerical index and create a new row with the data
        self.table = self.table.reset_index()
        line = pd.DataFrame(columns=self.table.columns, index=[position-0.5], data="")

        # Append the row to the end but keep its something-point-five index
        self.table = self.table.append(line, ignore_index=False)

        # Sort by index values to put the new row in the right place
        self.table = self.table.sort_index().reset_index(drop=True)

        # Rename the index back to its original name
        self.table.set_index("SDTLname", inplace=True)

        # Finally, signal to Qt that the row insertion process has ended
        self.endInsertRows()
        return True         # The row was inserted successfully

    def correct_type(self, value):
        if str(value).lower() == "true":
            return True
        elif str(value).lower() == "false":
            return False

        try:
            return int(value)
        except ValueError:
            try:
                return float(value)
            except ValueError:
                return value
        except TypeError:
            return value

    # More boilerplate

    def data(self, index, role=Qt.DisplayRole):
        column = index.column()
        row = index.row()

        if role == Qt.DisplayRole:
            return self.correct_type(self.table.values[row][column])
            #return self.table.values[row][column]
        elif role == Qt.BackgroundRole:
            return QColor(Qt.white)
        elif role == Qt.TextAlignmentRole:
            return Qt.AlignLeft

        return None
    
    def setData(self, index, value, role=Qt.EditRole):
        column = index.column()
        row = index.row()
        if(role == Qt.EditRole):
            self.table.iat[row, column] = self.correct_type(value)
            print(f"data set to {self.table.iat[row,column]}")
            self.dataChanged.emit(index, index)
            return True
        return False

    def flags(self, index):
        return Qt.ItemIsEnabled | Qt.ItemIsSelectable | Qt.ItemIsEditable

# The parameter view needs a special model to allow column insertion/removal
class ParameterTableModel(CustomTableModel):
    def __init__(self, data, state):
        CustomTableModel.__init__(self, data, state)

    # This is all pretty boilerplate, too

    def insertColumns(self, position, columns, parent):
        self.beginInsertColumns(QModelIndex(), position, position+columns-1)
        self.table.insert(position, "", None)
        self.endInsertColumns()
        return True

    def removeColumns(self, position, columns, parent):
        self.beginRemoveColumns(QModelIndex(), position, position+columns-1)
        self.table.drop(self.table.columns[position], axis=1, inplace=True)
        self.endRemoveColumns()
        return True

    def correct_type(self, value):
        if str(value).lower() == "true":
            return True
        elif str(value).lower() == "false":
            return False

        try:
            return int(value)
        except ValueError:
            try:
                return float(value)
            except ValueError:
                return value
        except TypeError:
            return value

    def data(self, index, role=Qt.DisplayRole):
        column = index.column()
        row = index.row()

        if role == Qt.DisplayRole:
            return self.correct_type(self.table.values[row][column])
        elif role == Qt.TextAlignmentRole:
            return Qt.AlignLeft

        return None

# Base class defining properties common to all of our widgets
class Widget(QWidget):
    def __init__(self, data, state):
        QWidget.__init__(self)

        # Set the model and delegate
        self.model = CustomTableModel(data, state)
        self.delegate = Delegate()

        # Create a the table view and define how to change it
        self.table_view = QTableView()
        self.table_view.setItemDelegate(self.delegate)
        self.table_view.setModel(self.model)
        self.table_view.setWordWrap(True)

        control_v = QtGui.QKeySequence(Qt.CTRL + Qt.Key_V)
        self.paste_shortcut = QtWidgets.QShortcut(control_v, self)
        self.paste_shortcut.setContext(Qt.WidgetShortcut)
        self.paste_shortcut.activated.connect(self.pasteSelection)
        #QtWidgets.QShortcut(QtGui.QKeySequence(Qt.CTRL + Q.Key_V), self).activated.connect(self.pasteSelection)

        # QTableView Headers
        self.horizontal_header = self.table_view.horizontalHeader()
        self.vertical_header = self.table_view.verticalHeader()

    def pasteSelection(self):
        print("PASTE")
        selection = self.selectedIndexes()
        if selection:
            text = QtWidget.qApp.clipboard().text()
            for index in selection:
                self.model.setData(index, text)

# The library widget is the view on the left side of the main window
# This is used to make changes that affect an entire function
class LibraryWidget(Widget):
    def __init__(self, data, state):
        Widget.__init__(self, data, state)
        self.state = state

        # Multiple selection is disallowed to prevent overlapping NSU views
        self.table_view.setSelectionMode(QAbstractItemView.SingleSelection)

        # Define what happens when a row is selected
        #self.vertical_header.sectionPressed.connect(self.row_selected)

        # Define what happens when a cell is selected
        self.table_view.selectionModel().selectionChanged.connect(self.cell_selected)

        # Create the main layout and add the table view to it
        self.main_layout = QHBoxLayout()
        self.main_layout.addWidget(self.table_view)

        # Set this as the main layout
        self.setLayout(self.main_layout)

        # True only during a row insertion or row removal operation
        self.row_change_mode = False

    # Turn on row change mode to ensure that views remain consistent
    def before_row_change(self):
        self.row_change_mode = True
        self.table_view.clearSelection()

    # Temporarily prevent selection to avoid overlap during row restructuring
    def during_row_change(self):
        self.table_view.selectionModel().selectionChanged.disconnect()
        self.table_view.setSelectionMode(QAbstractItemView.NoSelection)

    # Remove existing NSU widget, then reset the selection and selection mode
    def after_row_change(self):
        layout = self.main_layout
        if layout.count() > 1:
            item = layout.itemAt(layout.count() - 1)
            item.widget().close()
            layout.removeItem(item)
        self.nsu_widget.close()
        del self.nsu_widget
        self.table_view.setSelectionMode(QAbstractItemView.SingleSelection)
        self.table_view.selectionModel().selectionChanged.connect(self.cell_selected)
        self.table_view.clearSelection()
        self.row_change_mode = False

    # Define what happens when a row is inserted
    def insert(self):
        self.before_row_change()

        self.model.insertRow(self.selected_row)
        dt.sparse[self.state] = self.model.table

        self.during_row_change()

        dt.insert_row(self.selected_row, self.state)

        self.after_row_change()

    # Define what happens when a row is removed
    def remove(self):
        self.before_row_change()

        self.model.removeRow(self.selected_row)

        self.during_row_change()

        dt.delete_row(self.selected_row - 1, self.state)

        self.after_row_change()

    # Rename the function in all places it is referenced
    def rename(self, name):
        self.model.setHeaderData(self.selected_row, Qt.Vertical, name, Qt.EditRole)
        dt.row_nsu[self.state][self.selected_row]["SDTL"]["function_name"] = name

    # When a cell is selected, do the same thing as when its row is selected
    def cell_selected(self, item):
        indexes = item.indexes()
        for index in indexes:
            self.row_selected(index.row())

    # When a row is selected, create the NSU widget for that row...
    def row_selected(self, index):
        self.selected_row = index

        # ...unless a row change operation is in progress
        # (without this we'd have inconsistent views during the change)
        if self.row_change_mode:
            return

        nsu = dt.row_nsu[self.state][index]
        layout = self.main_layout

        # Remove previous NSU widget if present
        if layout.count() > 1:
            item = layout.itemAt(layout.count() - 1)
            item.widget().close()
            layout.removeItem(item)

        # Create the new NSU widget and add it to the layout
        self.nsu_widget = NSUWidget(nsu, self.state, index)
        layout.addWidget(self.nsu_widget)

# The Name, Syntax, URL view contains those items for each function variant
# Each NSU widget is associated only with the currently selected function
class NSUWidget(Widget):
    def __init__(self, data, state, row):
        Widget.__init__(self, data, state)

        self.state = state
        self.row = row
        self.parameter_index = None

        self.model = ParameterTableModel(data, state)
        self.table_view.setModel(self.model)
        self.table_view.setSelectionMode(QAbstractItemView.SingleSelection)

        self.layout = QVBoxLayout()
        self.layout.addWidget(self.table_view)
        self.setLayout(self.layout)

        # Define what happens when a column is selected
        self.horizontal_header.sectionPressed.connect(self.column_selected)

        # Define what happens when a cell is selected
        self.table_view.selectionModel().selectionChanged.connect(self.cell_selected)

    # Add a variant to this function with the given language tag
    def add_variant(self, language):
        columns = self.model.table.columns

        # Reverse iteration makes it easier to calculate the insertion point...
        for reverse_index, column in enumerate(reversed(columns)):

            # ...but we still need the forward index to actually insert there
            index = len(columns) - reverse_index - 1

            # If there's only one variant for this language, its name won't
            # have a number attached, so we need to attach numbers to both
            if column == language:
                self.model.setHeaderData(index, Qt.Horizontal, column + "_1", Qt.EditRole)
                self.model.insertColumn(index + 1)
                self.model.setHeaderData(index + 1, Qt.Horizontal, column + "_2", Qt.EditRole)
                dt.insert_parameters(self.row, index + 1, self.state)
                return

            # Otherwise we need to figure out what the highest number is
            # and insert after that point (hence the reverse iteration)
            elif language in column:
                language_index = column.split("_")[1]
                new_column = language + "_" + str(int(language_index) + 1)
                self.model.insertColumn(index + 1)
                self.model.setHeaderData(index + 1, Qt.Horizontal, new_column, Qt.EditRole)
                dt.insert_parameters(self.row, index + 1, self.state)
                return

        # If we got here, that means the given language doesn't exist yet
        # for this function, so we can just insert it at the end, no tricks
        index = len(columns)
        self.model.insertColumn(index)
        self.model.setHeaderData(index, Qt.Horizontal, language, Qt.EditRole)
        dt.insert_parameters(self.row, index, self.state)

    # Remove a variant from this function with the given language tag
    def remove_variant(self, language):
        columns = self.model.table.columns

        # If a function were to have no variants, why bother listing it at all?
        if len(columns) == 2:
            QMessageBox.about(self, "Unsupported Operation", "Each function must have at least one variant.")
            return

        # Reverse iteration so the variant can be removed from the right side
        for reverse_index, column in enumerate(reversed(columns)):

            # Once again, we still need the forward index
            index = len(columns) - reverse_index - 1

            # Only perform the removal if the given language is found
            if column == language or language in column:
                self.model.removeColumn(index)
                dt.remove_parameters(self.row, index, self.state)

                # If there were two variants, remove the index from the remnant
                if column != language:
                    language_index = column.split("_")[1]
                    if language_index == "2":
                        self.model.setHeaderData(index - 1, Qt.Horizontal, language, Qt.EditRole)
                return

        # If the language was not found, there's nothing to remove
        QMessageBox.about(self, "Unsupported Operation", language + " does not have any variants to remove.")

    # When a cell is selected, do the same thing as when its column is selected
    def cell_selected(self, item):
        indexes = item.indexes()
        for index in indexes:
            self.column_selected(index.column())

    # When a column is selected, create the parameter view for that variant
    def column_selected(self, index):
        self.parameter_index = index
        parameters = dt.row_parameters[self.state][self.row][index]

        # remove previous parameters if present
        if self.layout.count() > 1:
            item = self.layout.itemAt(self.layout.count() - 1)
            item.widget().close()
            self.layout.removeItem(item)

        # Create the parameter widget and add it to the layout
        self.parameter_widget = ParameterWidget(parameters, self.row, self.state, index)
        self.layout.addWidget(self.parameter_widget)

# This is a view into the parameters for the selected function variant only...
class ParameterWidget(Widget):
    def __init__(self, data, row, state, index):
        Widget.__init__(self, data, state)

        self.state = state
        self.row = row
        self.index = index

        self.model = ParameterTableModel(data, state)
        self.table_view.setModel(self.model)
        self.layout = QHBoxLayout()
        self.layout.addWidget(self.table_view)
        self.setLayout(self.layout)

    # ...except in the case of adding or removing columns, since it makes more
    # sense for those operation to affect parameters for all languages

    # Remove a parameter from all parameter data structures for this function
    def remove_parameter(self):

        # Pick the rightmost parameter to remove
        exp = dt.row_parameters[self.state][self.row][self.index].columns[-1]

        # Self-explanatory
        if exp == "EXP1":
            QMessageBox.about(self, "Unsupported Operation", "All functions must have at least one parameter.")
            return

        # Drop the parameter from all relevant parameter dataframes
        for parameters in dt.row_parameters[self.state][self.row]:
            parameters.drop(exp, axis=1, inplace=True)

        # Resetting the model works better than calling removeColumn()
        self.model = ParameterTableModel(dt.row_parameters[self.state][self.row][self.index], self.state)
        self.table_view.setModel(self.model)

    # Add a parameter to all parameter data structures for this function
    def add_parameter(self):

        # The new EXP# should be one more than the current highest
        exp_number = len(dt.row_parameters[self.state][self.row][self.index].columns)
        exp = "EXP" + str(exp_number + 1)

        # Insert a column named EXP# to all parameter dataframes
        for parameters in dt.row_parameters[self.state][self.row]:
            parameters[exp] = None

        # Resetting the model works better than calling insertColumn()
        self.model = ParameterTableModel(dt.row_parameters[self.state][self.row][self.index], self.state)
        self.table_view.setModel(self.model)

# A delegate allows us to define when the user is allowed to edit cell contents
class Delegate(QtWidgets.QItemDelegate):

    def createEditor(self, parent, option, index):
        return super(Delegate, self).createEditor(parent, option, index)

    # In this case, we want to let them change cell contents by typing
    # after double-clicking the cell OR typing when the cell is selected
    def setEditorData(self, editor, index):
        text = index.data(Qt.EditRole) or index.data(Qt.DisplayRole)
        editor.setText(str(text))

# Load the JSON document in the current directory by default
dt.read_json("SDTL_Function_Library.json")

# Create the Qt Application
app = QApplication([])

# Load the horizontal tab by default (looks better than loading nothing)
widget = LibraryWidget(dt.sparse["horizontal"], "horizontal")
window = MainWindow(widget)
window.show()

# Run the main Qt loop
sys.exit(app.exec_())
